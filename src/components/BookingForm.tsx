import { CleaningLevel, Booking, TimeSpan, defaultBooking, FormErrorType } from "../types";
import LevelRadioBtn from "./LevelRadioBtn";
import {  FormEvent, useState } from "react";

//addBooking of blockedTimes er fra bookingpage.jsx
type FormProps = {
  addBooking: (booking: Booking) => void;
  blockedTimes: TimeSpan[];
};

// tar emot props i form av addbooking och blockedtimes, som är destrukturerade från formprops type - och renderar bokningsformuläret
function BookingForm({
  addBooking,
  blockedTimes,
}: FormProps) {

  const [formErrors, setFormErrors] = useState<FormErrorType>({
    cleaner: "",
    time: "",
    date: "",
  });
  const [newBooking, setNewBooking] = useState<Booking>(defaultBooking);

  // hanterar ändringar i formulär inputs, uppdaterar newBooking state baserat på värdena av inputsen
  const handleChange = (e: FormEvent<HTMLInputElement | HTMLSelectElement>) => {
    const { name, value } = e.currentTarget;
    setNewBooking((prevState: Booking) => {
      return {
        ...prevState,
        //om name (input-värde på elementet som ändras) inte är data behåller vi
        // verdien utan att ändra den, annars ändrar vi på datan från string til object (med Date metod())
        [name]: name !== "date" ? value : new Date(value),
      };
    });
  };

  // hanterar formulär inmatning. Validerar inputs och kollar om den valda tiden är blockerad 
  // --> lägger till bokningen om det inte finns fel
  const handleSubmit = (e: FormEvent):void => {
    e.preventDefault();

    const bookingDateString = new Date(newBooking.date).toLocaleDateString();
    const dateWithTime = new Date(`${bookingDateString}T${newBooking.time}`);
    const errors = validateForm();
    //kollar om den valda tiden är blockerad av existerande bokningar
    if (timeIsBlocked(dateWithTime, newBooking.cleaner)) {
      errors.date = "Tiden du har valt är upptagen";
    }
    setFormErrors(errors);
    // sender newbooking til server og endrer state om det ikke finnes errors
    if (Object.values(errors).every((val) => val === "")) {
      addBooking({ ...newBooking, date: dateWithTime });
      setNewBooking(defaultBooking);
    }
  };
  

  const timeIsBlocked = (timeToBeCompared: Date, cleaner: string): boolean => {
    //if time finns i blocked times returnera false annars true
    let returnVal = false;
    blockedTimes.forEach((item) => {
      const start = new Date(item.from);
      const stop = new Date(item.to);
      if (
        cleaner === item.cleaner &&
        timeToBeCompared.getTime() > start.getTime() - 3 * 60 * 60 * 1000 &&
        timeToBeCompared.getTime() < stop.getTime()
      ) {
        returnVal = true;
      }
    });
    return returnVal;
  };
 
  const validateForm = ():FormErrorType => {
    const errors: FormErrorType = { cleaner: "", date: "", time: "" };
    if (!newBooking.cleaner) {
      errors.cleaner = "Du måste välja en städare";
    }

    if (!newBooking.time) {
      errors.time = "Du måste välja tid";
    }

    if (!newBooking.date) {
      errors.date = "Du måste välja datum";
    }
    return errors;
  };

  return (
    <form
      onSubmit={handleSubmit}
      className={
        Object.values(formErrors)
          .every((val) => val === "")
          .toString() + " booking-form gradient-border"
      }
    >
      <h2 className="form-title">Boka ny städning</h2>
      <div className="cleaner flex-col">
        <label htmlFor="cleaner">Välj Städare</label>
        <select
          name="cleaner"
          className={(formErrors.cleaner === "").toString() + " form-element"}
          value={newBooking.cleaner}
          onChange={handleChange}
        >
          <option value={""} hidden>
            Välj Städare
          </option>
          <option>Doris</option>
          <option>Boris</option>
          <option>Britta</option>
        </select>
        {formErrors.cleaner !== "" && (
          <p className="error">{formErrors.cleaner}</p>
        )}
      </div>

      <div className="lower-lvl">
        <div className="flex-col">
          <label htmlFor="level">Välj nivå</label>
          <div className="levels">
            {Object.keys(CleaningLevel)
              //!isNaN === number (true)
              //Number(val) -> konverterer value til number
              //Vi returnerer bare siffer og tar bort names, object.keys returnerer en array der radioBtn lagres
              .filter((val) => !isNaN(Number(val)))
              .map((key) => {
                return (
                  <LevelRadioBtn
                    key={key}
                    handleChange={handleChange}
                    newBooking={newBooking}
                    level={key}
                  />
                );
              })}
          </div>
        </div>
        <div className="flex">
          <div className="flex-col">
            <label htmlFor="date">Välj datum</label>
            <input
              className={"form-element"}
              onChange={handleChange}
              onFocus={(e) => e.currentTarget.showPicker()}
              type="date"
              name="date"
              value={newBooking.date.toLocaleDateString()}
              min={new Date().toLocaleDateString()}
              max="2025-03-31"
            />
          </div>
          <div className="flex-col">
            <label htmlFor="time">Välj tid</label>
            <input
              className={(formErrors.time === "").toString() + " form-element"}
              onFocus={(e) => e.currentTarget.showPicker()}
              onChange={handleChange}
              type="time"
              value={newBooking.time}
              name="time"
            />
            {formErrors.time !== "" && (
              <p className="error">{formErrors.time}</p>
            )}
          </div>
        </div>
      </div>
      <div className="submit-div">
        <button type="submit" id="book" className="submit-btn">
          Boka
        </button>
        {formErrors.date !== "" && (
          <p className="date-error">{formErrors.date}</p>
        )}
      </div>
    </form>
  );
}
export default BookingForm;
