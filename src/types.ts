export enum CleaningLevel {
  BASIC,
  TOPP,
  DIAMOND,
  WINDOW,
}

type CleaningLevel2 = "Basic"|"Topp"|"Diamant"|"Fönster"

const test:CleaningLevel2="Diamant"

export type Booking = {
  id: string;
  date: Date;
  time: string;
  level: CleaningLevel;
  client: string;
  cleaner: string;
  status: boolean;
};

export type TimeSpan = {
  cleaner: string;
  from: Date;
  to: Date;
};

export const getName = (num: number): string => {
  if (num == 0) return "Basic";
  if (num == 1) return "Topp";
  if (num == 2) return "Diamant";
  if (num == 3) return "Fönster";

  return "fel";
};

export const defaultBooking: Booking = {
  id: "",
  date: new Date(),
  time: "",
  level: CleaningLevel.BASIC,
  client: "Lasse",
  cleaner: "",
  status: false,
};

export type FormErrorType = {
  cleaner: string;
  time: string;
  date: string;
};