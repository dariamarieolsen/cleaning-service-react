import { Booking, getName } from "../types";
import "../styles/booking.css";

type Props = {
  booking: Booking;
  deletePost: (id:string)=>void;
  completePost: (booking:Booking)=>void;
};

// main komponent funktion som visar bokning detaljer
// tar emot props (booking, deletePost, completePost) som är destrukturerade från props type
function BookingComponent({ booking, deletePost, completePost }: Props) {
  const levelConvertedToName = getName(booking.level);
  return (
    <>
      
        <span>{booking.cleaner} </span>
        <span>{new Date(booking.date).toLocaleDateString()} </span>
        <span>{booking.time} </span>
        <span>{levelConvertedToName} </span>

        {!booking.status && (
          <>
            <div className="btn-div">
              <button onClick={() => completePost(booking)}>✔</button>
              <button onClick={() => deletePost(booking.id)}>✖</button>
            </div>
          </>
        )}
      
    </>
  );
}
export default BookingComponent;

// Denna komponenten visar individuella bokningsdetaljer och ger en funktion för att markera en bokning som klar eller ta bort den.
