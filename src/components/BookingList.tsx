import { compareDesc } from "date-fns";
import BookingComponent from "./BookingComponent";
import { Booking } from "../types";
import { useState } from "react";

type Props = {
  bookings: Booking[];
  isComplete: boolean;
  deletePost: (id: string) => void;
  completePost: (booking: Booking) => void;
};

function BookingList({
  bookings,
  isComplete,
  deletePost,
  completePost,
}: Props) {

  //Lista useState med strängar som är boknings ID
  //När man klickar på checkboxen vill vi att bokningens id läggs i en lista
  //När vi trycker på deleteknappen vill vi gå igenom listan med boknings ID och köra deletePost på alla

  const [itemsToBeDeleted, setItemsToBeDeleted] = useState<string[]>([]);

  // toggles the selected items in list
  const handleCheck = (id: string): void => {
    setItemsToBeDeleted((prevList) =>
      prevList.includes(id)
        ? prevList.filter((item) => item !== id)
        : [...prevList, id]
    );
  };

  // deletes items when delete button is clicked
  const deleteAll = () => {
    itemsToBeDeleted.forEach((item) => deletePost(item));
    setItemsToBeDeleted([]); // clear the selected items after deleting
  };


  return (
    <>
      <ul className="booking-ul">
        {bookings
          .slice()
          .sort((a, b) => {
            // new Date lagrer verdien i et object
            return compareDesc(new Date(b.date), new Date(a.date));
          })

          .map((booking) => {
            //isComplete er false da vi skapte den false i input fra BookingPage

            // renderar en div med checkbox om kraven är uppfyllda
            if (isComplete === booking.status) {
              return (
                <li
                  key={booking.id}
                  className="list-wrapper booking-li gradient-border"
                >
                  {isComplete && (
                    <input
                      type="checkbox"
                      onChange={() => handleCheck(booking.id)}
                      checked={itemsToBeDeleted.includes(booking.id)}
                    />
                  )}

                  <BookingComponent
                    booking={booking}
                    deletePost={deletePost}
                    completePost={completePost}
                  />
                </li>
              );
            }
            return null;
          })}
      </ul>
      {isComplete  &&(
        <button className="delete-all-btn" onClick={deleteAll}>
          Delete
        </button>
      )}
    </>
  );
}
export default BookingList;
