import BookingPage from "./BookingPage";
import LandingPage from "./LandingPage";
import { BrowserRouter, Routes, Route } from "react-router-dom";


// Högsta nivån, leder användaren till rätt path 
// function App representerar hela appen

function App() {
  //npm install react-router-dom localforage match-sorter sort-by
  return (
    <BrowserRouter>
      <>
        <Routes>
          <Route exact path="/" element={<LandingPage />} />
          <Route exact path="/BookingPage" element={<BookingPage />} />
        </Routes>
      </>
    </BrowserRouter>
  );
}

export default App;

// BrowserRouter = wrappar hela appen och ger routingen sin funktionalitet
// Routes = definierar routes för olika vägar i appen
// Route = Specifierar en route tillsammans med komponenten 

// Overall, this code sets up routing for the React application using react-router-dom. 
// It defines two routes, one for the landing page and another for the booking page, and renders the appropriate component based on the URL path.