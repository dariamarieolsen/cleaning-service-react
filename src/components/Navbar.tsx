import { useState } from "react";
import "./../styles/navbar.css";
import stadafint_logo from "./../assets/icons/stadafint_logo.png";
import { BsCart2 } from "react-icons/bs";
import { HiOutlineBars3 } from "react-icons/hi2";
import {
  Box,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import HomeIcon from "@mui/icons-material/Home";
import InfoIcon from "@mui/icons-material/Info";
import CommentRoundedIcon from "@mui/icons-material/CommentRounded";
import PhoneRoundedIcon from "@mui/icons-material/PhoneRounded";
import CleaningServicesIcon from "@mui/icons-material/CleaningServices";
import WorkOutlineIcon from "@mui/icons-material/WorkOutline";
import { useNavigate, Link } from "react-router-dom";
import BtnSymbol from "./../assets/icons/BtnSymbol";
import CloseIcon from "@mui/icons-material/Close";
import HeartIcon from "@mui/icons-material/FavoriteBorder";

export default function Navbar() {
  const [openMenu, setOpenMenu] = useState(false);
  const menuOptions = [
    {
      text: "Tjänster",
      icon: <CleaningServicesIcon />,
    },
    {
      text: "Om oss",
      icon: <InfoIcon />,
    },
    {
      text: "Kontakt",
      icon: <PhoneRoundedIcon />,
    },
    {
      text: "Företag",
      icon: <WorkOutlineIcon />,
    },
  ];

  return (
    <>
      <header>
        <div className="nav-large">
        <Link className="" to={"../"}>
        <img className="logo" src={stadafint_logo}></img>
              </Link>
          

          <nav>
            <div className="nav-content hidemobile">
              <ul className="header-ul">
                <li>Tjänster</li>
                <li>Om Oss</li>
                <li>Kontakt</li>
                <li>Företag</li>
              </ul>
              <Link className="linkToBooking" to={"../BookingPage"}>
                <button className="login-btn">
                  {" "}
                  <BtnSymbol /> MINA SIDOR
                </button>
              </Link>
            </div>
          </nav>
        </div>

        <div className="nav-small">
          <nav className="home-container">
            <img className="logo" src={stadafint_logo}></img>

            <div className="nav-content">
              <Link className="linkToBooking" to={"BookingPage"}>
                <button className="login-btn">
                  {" "}
                  <BtnSymbol /> MINA SIDOR
                </button>
              </Link>
              <div
                style={{
                  fontSize: "2rem",
                  marginRight: ".5rem",
                }}
                className="navbar-menu-container"
              >
                <HiOutlineBars3 onClick={() => setOpenMenu(true)} />
              </div>
            </div>
            <Drawer
              open={openMenu}
              onClose={() => setOpenMenu(false)}
              anchor="right"
            >
              <Box
                sx={{ width: "100vw" }}
                role="presentation"
                onClick={() => setOpenMenu(false)}
                onKeyDown={() => setOpenMenu(false)}
              >
                <List>
                  <CloseIcon className="closeIcon"></CloseIcon>

                  {menuOptions.map((item) => (
                    <ListItem key={item.text} disablePadding>
                      <ListItemButton>
                        <ListItemIcon>{item.icon}</ListItemIcon>
                        <ListItemText primary={item.text} />
                      </ListItemButton>
                    </ListItem>
                  ))}
                  <HeartIcon sx={{width: "50px", height: "50px"}} className="heartIcon"></HeartIcon>
                </List>
              </Box>
            </Drawer>
          </nav>
        </div>
      </header>
    </>
  );
}
