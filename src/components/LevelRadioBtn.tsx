import { Booking, CleaningLevel, getName } from "../types";
import { FormEvent } from "react";
import {
  BasicIcon,
  WindowIcon,
  DiamondIcon,
  ToppIcon,
} from "../assets/icons/LevelIcons";
interface RadioBtnProps {
  handleChange: (e: FormEvent<HTMLInputElement | HTMLSelectElement>) => void;
  newBooking: Booking;
  level: string;
}

function LevelRadioBtn({ handleChange, newBooking, level }: RadioBtnProps) {
  const parsedInt = parseInt(level.toString());
  const fill = newBooking.level == parsedInt ? "#E4C12B" : "#FEFCF6";
  const icons: JSX.Element[] = [
    <BasicIcon fill={fill} />,
    <ToppIcon fill={fill} />,
    <DiamondIcon fill={fill} />,
    <WindowIcon fill={fill} />,
  ];

  return (
    <>
      <div className="radio-wrapper">
        {icons[parsedInt]}
        <input
          onChange={handleChange}
          type="radio"
          name="level"
          checked={newBooking.level == parsedInt}
          value={parsedInt}
        ></input>
        <p className={(newBooking.level == parsedInt).toString()}>
          {getName(parsedInt)}
        </p>
      </div>
    </>
  );
}
export default LevelRadioBtn;
